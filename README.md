# artifact-docker

[![pipeline status](https://gitlab.com/zilbuz/artifact-docker/badges/master/pipeline.svg)](https://gitlab.com/zilbuz/artifact-docker/commits/master)

Docker image for the artifact tool (<https://github.com/vitiral/artifact>).

## Image description
- User: `artifact` (UID: 1000, GID: 1000);
- Working directory: `/data`
- Entrypoint: `art`

## Usage

### Via docker run
The recommanded way of using this image is to bind mount your project's directory in `/data` and directly passing arguments to `art` via `docker run`:
```bash
# Assuming that pwd is my project's directory

# Artifact help
docker run --rm -v $(pwd):/data zilbuz/artifact:latest help
# You can also call the image without argument to get the help
docker run --rm -v $(pwd):/data zilbuz/artifact:latest

# Init pwd for artifact
docker run --rm -v $(pwd):/data zilbuz/artifact:latest init

# List and filter artifacts
docker run --rm -v $(pwd):/data zilbuz/artifact:latest ls

# Export
docker run --rm -v $(pwd):/data zilbuz/artifact:latest export html ./exported_artifacts
## The exported_artifacts directory has been created in pwd
ls ./exported_artifacts
```

At the moment it isn't possible to directly serve the artifact's web-ui from the container because the created server will only listen on 127.0.0.1, and as a result will be inaccessible from outside the container. (Or at least I don't know how to make it accessible, feel free to open an issue if you know how to do it so that I can update this file =]).

### On Gitlab CI
The image can directly be used on Gitlab CI, but the entrypoint has to be overwritten:
```yml
image:
  name: zilbuz/artifact:latest
  entrypoint: [""]
```

Once this is done, the `art` command can be used in scripts:
```yml
script:
  - art check
  - art ls
  - art export html ./exported_artifacts
```

Since the image is run with a regular user instead of root, please check that an eventual `before_script` isn't trying to install dependencies or do something else that require root permissions.

As a full example, here is a job that check the project for artifact errors and warnings while inhibiting a global `before_script`:
```yml
check_artifacts:
  stage: test
  image:
    name: zilbuz/artifact:latest
    entrypoint: [""]
  before_script: [""]
  script:
    - art check
```

## Troubleshooting

### ERROR: Permission denied (os error 13)
If your host user doesn't have 1000 as UID and GID, you will encounter "Permission denied" errors when using `art export` or `art fmt`. To resolve them, you have to use the `--user` option of docker run to change the UID and GID of the user inside the container so that it match those of your local user:
```bash
# Check you UID and GID
id
#> uid=1001(mylocaluser) gid=1001(mylocaluser) groupes=1001(mylocaluser)

# Call docker with these numbers as --user parameters
docker run --user 1001:1001 --rm -v $(pwd):/data zilbuz/artifact:latest fmt
```

## Image building arguments
The following arguments are available to drive the image build:
- `ARTIFACT_VERSION` and `ARTIFACT_SHA512_SUM`: respectively the version of artifact and the sha512 sum of the archive downloaded from Github. If those arguments are modified, the Dockerfile should probably be updated because a patch is applied to the source tree before building the application;
- `RUST_NIGHTLY_VERSION`: Date of the nightly version of rust to pull for building artifact (format: YYYY-MM-DD);
- `MDBOOK_VERSION`: Version of the mdbook dependency to use;
- `CARGO_WEB_VERSION`: Version of the cargo-web dependency to use.
