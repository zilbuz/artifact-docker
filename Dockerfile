FROM rust:1.32.0@sha256:5faf4d5b7830f3c624dae7ed2fcf80abfcf50486ab405f7b094cbbdc299dceac as builder

# Install dependencies
ARG MDBOOK_VERSION="0.2.3"
RUN cargo install --version ${MDBOOK_VERSION} mdbook
ARG CARGO_WEB_VERSION="0.6.23"
RUN cargo install --version ${CARGO_WEB_VERSION} cargo-web

# Download and extract artifact sources
ARG ARTIFACT_VERSION="2.1.5"
ARG ARTIFACT_TAG="v${ARTIFACT_VERSION}"
ARG ARTIFACT_SHA512_SUM="a111d930f9075bd7ae524d66e3e454858492994da609e7967749ccb5c7360f5dff5d5fdaf4738b559c6ab77e1a070ec16330ab21d90672cc652e4676559b51b9"
WORKDIR /rust/src/artifact
RUN wget "https://github.com/vitiral/artifact/archive/${ARTIFACT_TAG}.tar.gz" -O artifact.tar.gz
RUN echo "${ARTIFACT_SHA512_SUM} artifact.tar.gz" > artifact_checksum.txt
RUN sha512sum -c artifact_checksum.txt
RUN tar zxvf artifact.tar.gz

# Build artifact
WORKDIR artifact-${ARTIFACT_VERSION}
## Build artifact and copy binary to the parent directory so that it's in a directory without version number
RUN cargo-web deploy -p artifact-frontend --release --target=wasm32-unknown-unknown
RUN cargo build -p artifact-app --release
RUN cp target/release/art ../

# Build the final image with just the artifact tool
FROM debian:stretch-slim@sha256:15ff3a968745e30585ddd28bb9707139d91a215ba3d2730e9036dba513c5dc77

LABEL maintainer="Basile Desloges <basile.desloges@gmail.com>"
LABEL description="Docker image for the artifact tool (https://github.com/vitiral/artifact)"
LABEL repository="https://gitlab.com/zilbuz/artifact-docker"

## Copy artifact from the previous image
COPY --from=builder /rust/src/artifact/art \
    /usr/local/bin

## Create and use an unprivileged user with specific UID and GID
RUN groupadd -g 1000 artifact && useradd -M -g artifact -u 1000 artifact
USER artifact

## Setup image environment with a specific workdir, and artifact as an entrypoint with the "help" command as default
WORKDIR /data
ENTRYPOINT [ "/usr/local/bin/art" ]
CMD [ "help" ]